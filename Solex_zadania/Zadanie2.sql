﻿Select kh_Symbol as 'Platnik', sum(ob_WartBrutto) as 'SUMA Brutto' from dbo.vwDokumenty as vw Left Join kh__Kontrahent as kh on vw.dok_PlatnikId=kh.kh_Id
where LEFT(vw.dok_nrpelny,2) = 'FS' and MONTH(vw.dok_DataWyst) = MONTH(getdate()) -1 and YEAR(vw.dok_DataWyst) = YEAR(getdate())
group by kh_Symbol
