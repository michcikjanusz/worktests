using System;
using System.Collections.Generic;
using Xunit;

namespace Rabaty.Test
{
    public class PoprawRabatyTestsBledneDane
    {
        public  Dictionary<long, Dictionary<int, CenaPoziomu>> dictCenyBazowe;
        public PoprawRabatyTestsBledneDane()
        {
            var dictCenaPoziomu1 = new Dictionary<int, CenaPoziomu>
            {
                { 1, new CenaPoziomu
                    {
                        Netto = 10,
                        PoziomId =1,
                        ProduktId =1
                    }
                },
                { 2, new CenaPoziomu
                    {
                        Netto = 0.4M,
                        PoziomId =2,
                        ProduktId =1
                    }
                },
                { 3, new CenaPoziomu
                    {
                        Netto = 66,
                        PoziomId =2,
                        ProduktId =1
                    }
                }
            };
            var dictCenaPoziomu2 = new Dictionary<int, CenaPoziomu>
            {
     
                { 1, new CenaPoziomu
                    {
                        Netto = 10.9M,
                        PoziomId =3,
                        ProduktId =2
                    }
                },
                { 2, new CenaPoziomu
                    {
                        Netto =1,
                        PoziomId =4,
                        ProduktId =2
                    }
                },
                { 3, new CenaPoziomu
                    {
                        Netto =55,
                        PoziomId =4,
                        ProduktId =2
                    }
                }
            };
            dictCenyBazowe = new Dictionary<long, Dictionary<int, CenaPoziomu>>
            {
                {1, dictCenaPoziomu1 },
                {2, dictCenaPoziomu2 }

            };
        }
        [Fact]
        public void BrakWartosciWRabatachRoznePoziomyCeny()
        {
            //Arrange

            //Przygotwanie modeli do testow
            var listRabaty = new List<Rabat>
            {
                new Rabat
                {
                    ProduktId = 1,
                    PoziomCenyId = 1

                },
                new Rabat
                {
                    ProduktId = 1,
                    PoziomCenyId = 2
                },
                new Rabat
                {
                    ProduktId = 1,
                    PoziomCenyId = 1

                }
            };

            //Act
            var rabaty = new Rabaty();
            var poprawioneRabaty = rabaty.PoprawRabaty(listRabaty, dictCenyBazowe);

            Rabat rabat;
            poprawioneRabaty.TryGetValue(listRabaty[1].Id, out rabat);

            //Assert

            //Powinnien zwrocic najnizasza wartosc  CenyPoziomu
            Assert.True(rabat.Equals(listRabaty[1]));
            Assert.Single(poprawioneRabaty);
        }

        [Fact]
        public void PustyRabat_ZwrotException()
        {
            //Arrange

            //Przygotwanie modeli do testow
            var listRabaty = new List<Rabat>
            {
                new Rabat
                {

                }
            };

            //Act
            var rabaty = new Rabaty();
            Action act = () => rabaty.PoprawRabaty(listRabaty, dictCenyBazowe);
            //Assert
            var exception = Assert.Throws<Exception>(act);
        }



        [Fact]
        public void PustaCena()
        {
            //Arrange

            //Przygotwanie modeli do testow
            var listRabaty = new List<Rabat>
            {
                new Rabat
                {
                    ProduktId = 1,
                    PoziomCenyId = 1,
                    Wartosc = 0.10M
                },
                new Rabat
                {
                    ProduktId = 1,
                    PoziomCenyId = 2,
                    Wartosc = 0.5M

                },
                new Rabat
                {
                    ProduktId = 1,
                    PoziomCenyId = 1,
                    Wartosc = 0.2M

                }
            };
            dictCenyBazowe = null;
            //Act
            var rabaty = new Rabaty();
            var poprawioneRabaty = rabaty.PoprawRabaty(listRabaty, dictCenyBazowe);

            Rabat rabatp1 = null;
      

            poprawioneRabaty.TryGetValue(listRabaty[1].Id, out rabatp1);

            //Assert

            Assert.True(rabatp1.Equals(listRabaty[1]));

        }
    }
}
