using System;
using System.Collections.Generic;
using Xunit;

namespace Rabaty.Test
{
    public class PoprawRabatyTestsPoprawneDane
    {
        public readonly Dictionary<long, Dictionary<int, CenaPoziomu>> dictCenyBazowe;
        public PoprawRabatyTestsPoprawneDane()
        {
            var dictCenaPoziomu1 = new Dictionary<int, CenaPoziomu>
            {
                { 1, new CenaPoziomu
                    {
                        Netto = 10,
                        PoziomId =1,
                        ProduktId =1
                    }
                },
                { 2, new CenaPoziomu
                    {
                        Netto = 0.4M,
                        PoziomId =2,
                        ProduktId =1
                    }
                },
                { 3, new CenaPoziomu
                    {
                        Netto = 66,
                        PoziomId =2,
                        ProduktId =1
                    }
                }
            };
            var dictCenaPoziomu2 = new Dictionary<int, CenaPoziomu>
            {

                { 1, new CenaPoziomu
                    {
                        Netto = 10.9M,
                        PoziomId =3,
                        ProduktId =2
                    }
                },
                { 2, new CenaPoziomu
                    {
                        Netto =1,
                        PoziomId =4,
                        ProduktId =2
                    }
                },
                { 3, new CenaPoziomu
                    {
                        Netto =55,
                        PoziomId =4,
                        ProduktId =2
                    }
                }
            };
            dictCenyBazowe = new Dictionary<long, Dictionary<int, CenaPoziomu>>
            {
                {1, dictCenaPoziomu1 },
                {2, dictCenaPoziomu2 }

            };
        }
        [Fact]
        public void TakiSamProduktPoziomInnaWartosc_ZwrotWartosc5_SingleCollection()
        {
            //Arrange

            //Przygotwanie modeli do testow
            var listRabaty = new List<Rabat>
            {
                new Rabat
                {
                    ProduktId = 1,
                    PoziomCenyId = 2,
                    Wartosc = 0.5M

                },
                new Rabat
                {
                    ProduktId = 1,
                    PoziomCenyId = 2,
                    Wartosc = 0.1M
                }
            };

            //Act
            var rabaty = new Rabaty();
            var poprawioneRabaty = rabaty.PoprawRabaty(listRabaty, dictCenyBazowe);

            Rabat rabat;
            poprawioneRabaty.TryGetValue(listRabaty[0].Id, out rabat);

            //Assert

            //Powinnien zwrocic najwyzsza wartosc 
            Assert.True(rabat.Equals(listRabaty[0]));
            Assert.Single(poprawioneRabaty);
        }

        [Fact]
        public void InnyProduktInnyPoziomInnaWartosc_ZwrotMultiCollectionWyzszeWartosciRabatow()
        {
            //Arrange

            //Przygotwanie modeli do testow
            var listRabaty = new List<Rabat>
            {
                new Rabat
                {
                    ProduktId = 1,
                    PoziomCenyId = 1,
                    Wartosc = 0.5M

                },
                new Rabat
                {
                    ProduktId = 2,
                    PoziomCenyId = 3,
                    Wartosc = 0.2M
                },
                new Rabat
                {
                    ProduktId = 1,
                    PoziomCenyId = 2,
                    Wartosc = 0.84M

                },
                new Rabat
                {
                    ProduktId = 2,
                    PoziomCenyId = 4,
                    Wartosc = 0.929M
                },
                new Rabat
                {
                    ProduktId = 2,
                    PoziomCenyId = 3,
                    Wartosc = 0.29M
                },
                new Rabat
                {
                    ProduktId = 1,
                    PoziomCenyId = 1,
                    Wartosc = 0.14M

                }
            };



            //Act
            var rabaty = new Rabaty();
            var poprawioneRabaty = rabaty.PoprawRabaty(listRabaty, dictCenyBazowe);

            Rabat rabatp1 = null;
            Rabat rabatp2 = null;

            poprawioneRabaty.TryGetValue(listRabaty[2].Id, out rabatp1);
            poprawioneRabaty.TryGetValue(listRabaty[3].Id, out rabatp2);

            //Assert
            
            Assert.True(rabatp1.Equals(listRabaty[2]));
            Assert.True(rabatp2.Equals(listRabaty[3]));
            Assert.Equal(2, poprawioneRabaty.Count);
        }
    }
}
