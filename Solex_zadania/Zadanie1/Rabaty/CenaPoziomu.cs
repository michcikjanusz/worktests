﻿namespace Rabaty
{
    public class CenaPoziomu 
    {
        public static long wyliczKlucz(long produktId, int poziomId)
        {
            string klucz = produktId + "||" + poziomId;
            return klucz.GetHashCode();
        }

        public long Id => wyliczKlucz(this.ProduktId, this.PoziomId);

        public long ProduktId { get; set; }
        public int PoziomId { get; set; }
        public long? WalutaId { get; set; }
        public decimal Netto { get; set; }

        public CenaPoziomu(int poziomId, decimal netto, long productId, long? walutaId = null)
        {
            PoziomId = poziomId;
            Netto = netto;
            ProduktId = productId;
            WalutaId = walutaId;
        }

        public CenaPoziomu()
        {
        }
        public CenaPoziomu(CenaPoziomu baza)
        {
            PoziomId = baza.PoziomId;
            Netto = baza.Netto;
            ProduktId = baza.ProduktId;
            WalutaId = baza.WalutaId;
        }
        public bool RecznieDodany()
        {
            return PoziomId < 0 || ProduktId < 0;
        }

    }
}