﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Rabaty
{
    public class Rabaty
    {

        public Dictionary<long, Rabat> PoprawRabaty(List<Rabat> rabaty, Dictionary<long, Dictionary<int, CenaPoziomu>> cenybazowe)
        {
            //czy rabaty sa 0 - nie moze byc tak
            if (rabaty.Any(x => x.Id == 0 || (!x.Wartosc.HasValue && !x.PoziomCenyId.HasValue)))
            {
                throw new Exception("Rabaty zerowe ID!! Nie może być tak - zmiana mechanizmu liczenia ID dla rabatów - rabaty muszą mieć jawnie wyliczone ID.");
            }

            var unikalneRabaty = rabaty.GroupBy(x => x.Id).ToDictionary(x => x.Key, x => x.ToList());
            var wynik = new Dictionary<long, Rabat>(unikalneRabaty.Count);
            try 
            { 
                foreach (var r in unikalneRabaty)
                {

                    Rabat najlepszy = r.Value.OrderByDescending(x => x.Wartosc).First();

                    //Gdy ceny bazowe są nullem bierzemy po prostu najwyzsza wartosc

                    if (r.Value.Count > 1 && cenybazowe != null)
                    {
                        //szukamy najlepszego rabatu - albo po cenach - jak sa poziomy cenowe, albo po rabatach
                        //wystarczy ze sprawdzimy dla jednego rabatu, bo i tak mamy tu te same rabaty po ID - wiec maja takie same warunki, tylko inne wartości

                        cenybazowe.TryGetValue(najlepszy.ProduktId.Value, out Dictionary<int, CenaPoziomu> dict);
                        CenaPoziomu cenaNajlepszego = dict.Values.Where(x=>x.PoziomId == najlepszy.PoziomCenyId.Value)
                                                                 .OrderBy(x=>x.Netto).First();

                        //Jezeli sa 2 poziomy do jednego Rabatu bierzemy najnizsza wartosc -- tego nie jestem do konca pewny, bo nie wiem czy jest to logiczne... - ale zakladam ze sie tak da :)

                        if (najlepszy.ProduktId.HasValue && najlepszy.PoziomCenyId.HasValue && cenaNajlepszego !=null)
                        {
                            decimal? cenaPoRabacieNajlepszego = najlepszy.Wartosc.GetValueOrDefault(1) * cenaNajlepszego.Netto;

                            //Lepiej nie literowac dla warunkow gdzie mamy ten sam poziom ceny

                            foreach (Rabat rabat in r.Value.Where(x=>x.PoziomCenyId.Value != najlepszy.PoziomCenyId.Value))
                            {

                                cenybazowe.TryGetValue(rabat.ProduktId.Value, out Dictionary<int, CenaPoziomu> dictRabat);
                                CenaPoziomu cena = dictRabat.Values.Where(x => x.PoziomId == rabat.PoziomCenyId.Value)
                                                                         .OrderBy(x => x.Netto).First();
                                //Jezeli sa 2 poziomy do jednego Rabatu bierzemy najnizsza wartosc -- tego nie jestem do konca pewny, bo nie wiem czy jest to logiczne... - ale zakladam ze sie tak da :)

                                //czy jest poziom cenowy i jest INNY niż ten z najlepszego rabatu
                                if (cenaPoRabacieNajlepszego.HasValue && rabat.PoziomCenyId.HasValue && najlepszy.PoziomCenyId.HasValue && cena != null)
                                {
                                    decimal porabacie = cena.Netto * rabat.Wartosc.GetValueOrDefault(1);

                                    //jest poziom cenowy, ale inny niz najlepszego
                                    if (cenaPoRabacieNajlepszego > porabacie)
                                    {
                                        //zmiana
                                        najlepszy = rabat;
                                        cenaPoRabacieNajlepszego = porabacie;
                                    }
                                }
                                // Rabat mamy wybrany najwiekszy wiec nie ma sensu tego sprawdzac

                                //else
                                //{
                                //    //nie ma poziomu w ogole, albo sa takie same - sprawdzenie po rabacie tylko
                                //    if (najlepszy.Wartosc.GetValueOrDefault(1) < rabat.Wartosc.GetValueOrDefault(1))
                                //    {
                                //        //zmiana
                                //        najlepszy = rabat;
                                //        cenaPoRabacieNajlepszego = null;
                                //    }
                                //}
                            }
                        }
                        // Wyznaczamy od razu najwyzsza wartosc rabatu, nie ma potrzeby literowania po rabatach i przypisywania.

                        //else
                        //{
                        //    foreach (Rabat rabat in r.Value)
                        //    {
                        //        if (najlepszy.Wartosc.GetValueOrDefault(1) < rabat.Wartosc.GetValueOrDefault(1))
                        //        {
                        //            //zmiana
                        //            najlepszy = rabat;
                        //        }
                        //    }
                        //}
                    }
                    // Wydaje mi sie ze nie ma potrzeby sprawdzania jeszcze raz czy dany rabat jest nullem czy zerem, skoro jest to sprawdzane na poczatku.

                    //if (najlepszy == null || najlepszy.Id == 0 ||)
                    //{
                    //    throw new Exception("Pusty rabat!");
                    //}

                    //tylko najlepsze dodajemy do kolekcji
                    wynik.Add(najlepszy.Id, najlepszy);
                }
            }
            catch(Exception ex)
            {
                var msgError = ex.Message;
            }



            return wynik;
        }
    }
}
