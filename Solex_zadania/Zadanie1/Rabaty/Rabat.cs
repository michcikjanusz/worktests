﻿using System;

namespace Rabaty
{
    public enum RabatSposob : short
    {
        MinusCena = 1,
        StalaCena = 2,
        Procentowy = 0
    }

    public enum RabatTyp : short
    {
        Zaawansowany = 1,
        Promocja = -3,
        Prosty = 3,
        RabatDodatkowyDoliczanyDoTegoCoMaKlient = 4
    }

    public class Rabat
    {
        public Rabat()
        {
            Aktywny = true;
            TypWartosci = RabatSposob.Procentowy;
            TypRabatu = RabatTyp.Prosty;
            this.DodanyWAdminie = true;
        }

        public decimal? Wartosc { get; set; }

        public bool DodanyWAdminie { get; set; }

        public RabatSposob TypWartosci { get; set; }

        public DateTime? OdKiedy { get; set; }

        public DateTime? DoKiedy { get; set; }

        public long? KlientId { get; set; }

        public int? KategoriaKlientowId { get; set; }

        public long? KategoriaProduktowId { get; set; }

        public long? ProduktId { get; set; }

        public long? CechaId { get; set; }

        public string DodanyPrzez { get; set; }

        public long? WalutaId { get; set; }

        public bool Aktywny { get; set; }

        public RabatTyp TypRabatu { get; set; }

        public int? PoziomCenyId { get; set; }

        public long Id => this.WyliczId();

        public static long WyliczID(RabatTyp typ, long? klient, int? kategoriaKlienta, long? produktId, long? kategorieProduktow, long? cecha, long? waluta)
        {
            return (string.Concat(typ, "_", klient, "_", kategoriaKlienta, "_", produktId, "_", kategorieProduktow, "_", cecha, "_", waluta)).GetHashCode();
        }

        public long WyliczId(int znak = 0)
        {
            return Rabat.WyliczID(this.TypRabatu, this.KlientId, this.KategoriaKlientowId, this.ProduktId, this.KategoriaProduktowId, this.CechaId, this.WalutaId);
        }

        public bool RecznieDodany() { return this.DodanyWAdminie; }
    }
}