﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Zadanie3.Domain;

namespace Zadanie3.Data
{
    public interface IZadanie3Repository
    {
        Task<Dictionary<int, double>> PobierzKoszyk();
        Task<double> PobierzKoszykWartosc();

        Task<List<Zamowienie>> UsunZKoszyka(int idProduktu);

        Task<Zamowienie> DodajDoKoszyka(int idProduktu, double iloscProduktu);

        Task<List<Zamowienie>> PobierzZamowienia(int idProduktu);


        Task<int> Commit();

    }
}
