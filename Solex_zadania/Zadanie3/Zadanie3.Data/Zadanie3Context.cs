﻿using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Text;
using Zadanie3.Domain;

namespace Zadanie3.Data
{
    public class Zadanie3Context : DbContext
    { 
            public Zadanie3Context(DbContextOptions<Zadanie3Context> options) : base(options)
            {
            }
        public DbSet<Zamowienie> Zamowienies { get; set; }
        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Zamowienie>()
                .HasKey(k => k.Id);
        }
        
    }
}
