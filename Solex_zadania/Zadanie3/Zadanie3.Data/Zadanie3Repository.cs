﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Zadanie3.Domain;

namespace Zadanie3.Data
{
    public class Zadanie3Repository : IZadanie3Repository
    {
        private readonly Zadanie3Context _zadanie3Context;
        private readonly ILogger<Zadanie3Repository> _logger;

        public Zadanie3Repository(Zadanie3Context zadanie3Context, ILogger<Zadanie3Repository> logger)
        {
            _zadanie3Context = zadanie3Context;
            _logger = logger;
        }

        public async Task<int> Commit()
        {
            _logger.LogInformation("Commiting");

            return await _zadanie3Context.SaveChangesAsync();
        }

        public async Task<Zamowienie> DodajDoKoszyka(int idProduktu, double iloscProduktu)
        {
            _logger.LogInformation($"Dodawanie do koszyka{idProduktu} w ilosci {iloscProduktu}");
            var noweZamowienie = new Zamowienie()
            {
                Ilosc = iloscProduktu,
                ProduktId = idProduktu
            };
            await _zadanie3Context.Zamowienies.AddAsync(noweZamowienie);

            return noweZamowienie;
        }

        public async Task<Dictionary<int, double>> PobierzKoszyk()
        {
            _logger.LogInformation("Pobieranie Koszyka");
            var koszyk = await _zadanie3Context.Zamowienies.GroupBy(w => w.ProduktId)
                .Select(c => new
                {
                    ProduktId = c.Key,
                    Ilosc = c.Sum(i => i.Ilosc)
                }).ToDictionaryAsync(k=>k.ProduktId,v=>v.Ilosc);
            return koszyk;
        }

        public async Task<double> PobierzKoszykWartosc()
        {
            _logger.LogInformation($"Pobieranie Wartosci Koszyka");
            double cena = 0;
            var caloscList = await _zadanie3Context.Zamowienies.GroupBy(p => p.ProduktId).Select(c => new
            {
                ProduktId = c.Key,
                Ilosc = c.Sum(i => i.Ilosc)
            }).ToListAsync();

            foreach (var produktGrupa in caloscList)
            {
                //Dodaje >= 10, bo 10 robi nam blad
                if (produktGrupa.Ilosc >= 10)
                {
                    cena += 10 * produktGrupa.Ilosc;
                }
                else if (produktGrupa.Ilosc < 10 && produktGrupa.Ilosc > 0)
                {
                    cena += 5 * produktGrupa.Ilosc;
                }
                else
                {
                    cena += 0;
                }
                
            }

            return cena;
        }

        public async Task<List<Zamowienie>> PobierzZamowienia(int idProduktu)
        {
            _logger.LogInformation($"Pobieranie Zamowienia{idProduktu}");

            var zamowienie = _zadanie3Context.Zamowienies.Where(z => z.ProduktId == idProduktu);

            return await zamowienie.ToListAsync();
        }

        public async Task<List<Zamowienie>> UsunZKoszyka(int idProduktu)
        {
            _logger.LogInformation($"Pobieranie Zamowienia{idProduktu}");
            _zadanie3Context.RemoveRange(await PobierzZamowienia(idProduktu));
            return await PobierzZamowienia(idProduktu);
        }
    }
}
