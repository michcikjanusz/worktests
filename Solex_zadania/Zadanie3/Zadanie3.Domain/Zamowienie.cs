﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Zadanie3.Domain
{
    public class Zamowienie
    {
        [Key]
        public int Id { get; set; }
        public int ProduktId { get; set; }
        public double Ilosc { get; set; }

    }
}
 