﻿using FakeItEasy;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using Xunit;
using Zadanie3.Controllers;
using Zadanie3.Data;
using Zadanie3.Domain;

namespace Zadanie3.Test
{
    public class RepositoryTest : ConfigurationTest
    {
        [Fact]
        public async Task PobierzKoszyk()
        {
            //Arrange
            using (var context = new Zadanie3Context(options))
            {
                context.Database.OpenConnection();
                context.Database.EnsureCreated();
                var zadanie3Repository = new Zadanie3Repository(context, loggerRepo);
                await zadanie3Repository.DodajDoKoszyka(1, 10);
                await zadanie3Repository.Commit();
                var dict = new Dictionary<int, double>
                {
                    {1,10}
                };
                // Act
                var wynik = await zadanie3Repository.PobierzKoszyk();
                //assert

                Assert.Equal(dict, wynik);

                context.Database.CloseConnection();
                context.Database.EnsureDeleted();
            }

        }

        [Fact]
        public async Task UsunZamowienie()
        {
            //Arrange
            using (var context = new Zadanie3Context(options))
            {
                context.Database.OpenConnection();
                context.Database.EnsureCreated();
                var zadanie3Repository = new Zadanie3Repository(context, loggerRepo);
                var zamowienie =  await zadanie3Repository.DodajDoKoszyka(1, 10);
                var listZamowien = new List<Zamowienie>();
                listZamowien.Add(zamowienie);
                await zadanie3Repository.Commit();
               
                // Act
                var wynik = await zadanie3Repository.UsunZKoszyka(1);
                //assert

                Assert.Equal(listZamowien, wynik);

                context.Database.CloseConnection();
                context.Database.EnsureDeleted();
            }

        }
    }
}
