﻿using FakeItEasy;
using Microsoft.Data.Sqlite;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Text;
using Zadanie3.Controllers;
using Zadanie3.Data;

namespace Zadanie3.Test
{
    public class ConfigurationTest
    {
        public readonly DbContextOptions<Zadanie3Context> options;
        public readonly ILogger<KoszykController> loggerKoszyk;
        public readonly ILogger<Zadanie3Repository> loggerRepo;
        public ConfigurationTest()
        {
            var connectionStringBuilder = new SqliteConnectionStringBuilder { DataSource = ":memory:" };
            var connection = new SqliteConnection(connectionStringBuilder.ToString());

            options = new DbContextOptionsBuilder<Zadanie3Context>()
                .UseSqlite(connection)
                .Options;
            loggerKoszyk = A.Fake<ILogger<KoszykController>>();
            loggerRepo = A.Fake<ILogger<Zadanie3Repository>>();

        }
    }
}
