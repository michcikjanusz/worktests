using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Xunit;
using Zadanie3.Controllers;
using Zadanie3.Data;
using Zadanie3.Domain;

namespace Zadanie3.Test
{
    public class ControllerTest : ConfigurationTest
    {
        [Fact]
        public async Task PobierzKOszyk_Return_200()
        {
            //Arrange
            using (var context = new Zadanie3Context(options))
            {
                context.Database.OpenConnection();
                context.Database.EnsureCreated();
                var zadanie3Repository = new Zadanie3Repository(context, loggerRepo);
                await zadanie3Repository.DodajDoKoszyka(1, 10);
                await zadanie3Repository.Commit();
                KoszykController koszykController = new KoszykController( loggerKoszyk, zadanie3Repository);
                // Act
                var result = await koszykController.PobierzKoszyk();
                var createdResult = result.Result as ObjectResult;
                var statusCode = createdResult.StatusCode.GetValueOrDefault();
                //assert

                Assert.Equal(200, statusCode);

                context.Database.CloseConnection();
                context.Database.EnsureDeleted();
               
            }
        }
        [Fact]
        public async Task PobierzKoszykWartosc_Return200orazValue()
        {
            //Arrange
            using (var context = new Zadanie3Context(options))
            {
                context.Database.OpenConnection();
                context.Database.EnsureCreated();
                var zadanie3Repository = new Zadanie3Repository(context, loggerRepo);
                await zadanie3Repository.DodajDoKoszyka(1, 10);
                await zadanie3Repository.Commit();
                KoszykController koszykController = new KoszykController(loggerKoszyk, zadanie3Repository);
                // Act
                var result = await koszykController.PobierzKoszykWartosc();
                var createdResult = result.Result as ObjectResult;
                var statusCode = createdResult.StatusCode.GetValueOrDefault();
                //assert

                Assert.Equal(200, statusCode);
                Assert.Equal(100.00, createdResult.Value);


                context.Database.CloseConnection();
                context.Database.EnsureDeleted();

            }
        }
        [Fact]
        public async Task UsunZamowienie_Return200orazUsunieteObiekty()
        {
            //Arrange
            using (var context = new Zadanie3Context(options))
            {
                context.Database.OpenConnection();
                context.Database.EnsureCreated();
                var zadanie3Repository = new Zadanie3Repository(context, loggerRepo);
                var zamowienie1 = await zadanie3Repository.DodajDoKoszyka(1, 10);
                var zamowienie2 = await zadanie3Repository.DodajDoKoszyka(1, 66.77);
                await zadanie3Repository.Commit();

                var listZamowien = new List<Zamowienie>();
                listZamowien.Add(zamowienie1);
                listZamowien.Add(zamowienie2); 
                KoszykController koszykController = new KoszykController(loggerKoszyk, zadanie3Repository);
                // Act
                var result = await koszykController.UsunZKoszyka(1);
                var createdResult = result.Result as ObjectResult;
                var statusCode = createdResult.StatusCode.GetValueOrDefault();
                //assert

                Assert.Equal(200, statusCode);
                Assert.Equal(listZamowien, createdResult.Value);


                context.Database.CloseConnection();
                context.Database.EnsureDeleted();

            }
        }
    }
}
