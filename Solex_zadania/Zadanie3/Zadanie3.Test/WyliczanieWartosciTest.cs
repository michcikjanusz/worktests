using Microsoft.EntityFrameworkCore;
using System;
using System.Threading.Tasks;
using Xunit;
using Zadanie3.Data;

namespace Zadanie3.Test
{
    public class WyliczanieWartosciTest : ConfigurationTest
    {
        [Fact]
        public async Task WyliczanieWartosci_zwrot100()
        {
            //Arrange
            using (var context = new Zadanie3Context(options))
            {
                context.Database.OpenConnection();
                context.Database.EnsureCreated();
                var zadanie3Repository = new Zadanie3Repository(context, loggerRepo);
                await zadanie3Repository.DodajDoKoszyka(1, 10);
                await zadanie3Repository.Commit();
                // Act
                var wynik = await zadanie3Repository.PobierzKoszykWartosc();
                //assert
                Assert.Equal(100, wynik);

                context.Database.CloseConnection();
                context.Database.EnsureDeleted();
            }

        }

        [Fact]
        public async Task WyliczanieWartosci_zwrot25()
        {
            //Arrange
            using (var context = new Zadanie3Context(options))
            {
                context.Database.OpenConnection();
                context.Database.EnsureCreated();
                var zadanie3Repository = new Zadanie3Repository(context, loggerRepo);
                await zadanie3Repository.DodajDoKoszyka(1, 5);
                await zadanie3Repository.Commit();
                // Act
                var wynik = await zadanie3Repository.PobierzKoszykWartosc();
                //assert
                Assert.Equal(25, wynik);

                context.Database.CloseConnection();
                context.Database.EnsureDeleted();
            }

        }

        [Fact]
        public async Task WyliczanieWartosci_zwrot0_dlaBlednychdanych()
        {
            //Arrange
            using (var context = new Zadanie3Context(options))
            {
                context.Database.OpenConnection();
                context.Database.EnsureCreated();
                var zadanie3Repository = new Zadanie3Repository(context, loggerRepo);
                await zadanie3Repository.DodajDoKoszyka(1, -5);
                await zadanie3Repository.DodajDoKoszyka(1, 0);
                await zadanie3Repository.Commit();
                // Act
                var wynik = await zadanie3Repository.PobierzKoszykWartosc();
                //assert
                Assert.Equal(0, wynik);

                context.Database.CloseConnection();
                context.Database.EnsureDeleted();
            }

        }

        [Fact]
        public async Task WyliczanieWartosci_zwrot125()
        {
            //Arrange
            using (var context = new Zadanie3Context(options))
            {
                context.Database.OpenConnection();
                context.Database.EnsureCreated();
                var zadanie3Repository = new Zadanie3Repository(context, loggerRepo);
                await zadanie3Repository.DodajDoKoszyka(1, 5);
                await zadanie3Repository.DodajDoKoszyka(2, 10);
                await zadanie3Repository.Commit();
                // Act
                var wynik = await zadanie3Repository.PobierzKoszykWartosc();
                //assert
                Assert.Equal(125, wynik);

                context.Database.CloseConnection();
                context.Database.EnsureDeleted();
            }

        }
    }
}
