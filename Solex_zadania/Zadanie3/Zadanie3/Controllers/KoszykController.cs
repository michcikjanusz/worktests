﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Routing;
using Microsoft.Extensions.Logging;
using Zadanie3.Data;
using Zadanie3.Domain;

namespace Zadanie3.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class KoszykController : ControllerBase
    {
       

        private readonly ILogger<KoszykController> _logger;
        private readonly IZadanie3Repository _repository;

        public KoszykController(ILogger<KoszykController> logger, IZadanie3Repository repository)
        {
            _repository = repository;
            _logger = logger;
        }

        [HttpGet("PobierzKoszyk")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<Dictionary<int,double>>> PobierzKoszyk()
        {
            try
            {
                var result = await _repository.PobierzKoszyk();

                if (result == null) return NotFound("Twoj koszyk jest pusty");

                return Ok(result);

            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                return this.StatusCode(StatusCodes.Status500InternalServerError, "Blad Bazy, Sprobuj ponownie za chwile");
            }
        }

        [HttpDelete("UsunZKoszyka/{idProduktu}")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<List<Zamowienie>>> UsunZKoszyka([Required, Range(1,10000)] int idProduktu)
        {
            try
            {
                var result = await _repository.UsunZKoszyka(idProduktu);
                await _repository.Commit();

                if (result == null) return NotFound($"Nie znaleziono podanego id{idProduktu}");

                return Ok(result);

            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                return this.StatusCode(StatusCodes.Status500InternalServerError, "Blad Bazy, Sprobuj ponownie za chwile");
            }
        }

        [HttpPost("DodajDoKoszyka")]
        [ProducesResponseType(StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status400BadRequest)]
        public async Task<ActionResult<Zamowienie>> DodajDoKoszyka([Required, Range(1, 10000)]int idProduktu,[Required] double iloscProduktu)
        {
            try
            {
                var result = await  _repository.DodajDoKoszyka(idProduktu, iloscProduktu);
                await _repository.Commit();

                if (result == null) return BadRequest("Upewnij sie czy podales wszystkie dane i czy sa podane w poprawnym formacie");

                return Ok(result);

            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                return this.StatusCode(StatusCodes.Status500InternalServerError, "Blad Bazy, Sprobuj ponownie za chwile");
            }
        }

        [HttpGet("PobierzKoszykWartosc")]
        public async Task<ActionResult<double>> PobierzKoszykWartosc()
        {
            try
            {
                var result = await _repository.PobierzKoszykWartosc();

                return Ok(result);

            }
            catch (Exception ex)
            {
                _logger.LogError(ex.Message);
                return this.StatusCode(StatusCodes.Status500InternalServerError, "Blad Bazy, Sprobuj ponownie za chwile");
            }
        }
    }
}
